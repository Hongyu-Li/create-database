
-- story 1
create table client (
    clientId int PRIMARY KEY NOT null,
    fullName varchar(128),
    abvName varchar(6)
);

-- story 2
-- one way
CREATE TABLE contract (
    id int PRIMARY KEY NOT null,
    companyName varchar(64),
    companyId int
);

ALTER TABLE contract
ADD CONSTRAINT FK_CLIENT_CONTRACT
FOREIGN KEY (companyId) REFERENCES client(clientId)


-- another way
CREATE TABLE contract (
    id int PRIMARY KEY NOT null,
    companyName varchar(64),
    companyId int,
    FOREIGN KEY (companyId) REFERENCES client(clientId)
);

-- story 3
alter table contract
add column serviceId int DEFAULT 1 not null;

create table service (
    serviceId int Primary Key DEFAULT 1 not null,
    type varchar(10) DEFAULT "MOVIE"
);

alter table contract
add CONSTRAINT FK_CONTRACT_SERVICE
FOREIGN KEY (serviceId) REFERENCES service(serviceId);

-- story 4
create table office (
    officeId int PRIMARY KEY NOT null,
    city varchar(32),
    country varchar(32)
);

create table employees (
    employeeId int PRIMARY KEY NOT NULL,
    firstName varchar(32),
    lastName varchar(32),
    officeId int
);

alter table employees
add CONSTRAINT FK_EMPLOYEES_OFFICE
FOREIGN key (officeId) REFERENCES office(officeId);

-- story 5
create table contract_service_office(
    contractId int,
    officeId int,
    PRIMARY key (contractId, officeId)
);

alter table contract_service_office
add CONSTRAINT FK_contract_office
FOREIGN key (officeId) REFERENCES office(officeId);

alter table contract_service_office
add CONSTRAINT FK_CONTRACT_OFFICE_SERVICE
FOREIGN KEY (contractId) REFERENCES contract(id);

-- story 6
alter table contract_service_office
add column adminId int;

alter table contract_service_office
add CONSTRAINT FK_office_employee
FOREIGN KEY (adminId) REFERENCES employees(employeeId);





























